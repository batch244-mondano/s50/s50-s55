import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home'
import './App.css'; 

function App() {
  return (
    <>
      <AppNavbar/>
      <Home/>
    </>
  );
}

export default App;
