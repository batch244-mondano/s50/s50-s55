import { Row, Col, Card, Button } from 'react-bootstrap';

export default function CourseCard() {
	return (
		<Row>
			<Col>
				<Card>
						<Card.Body>
							<Card.Title>Sample Course</Card.Title>
							<Card.Text>
								<h5 className="mb-0">Description:</h5>
								<p1>This is a sample course offering.</p1>
							</Card.Text>
							<Card.Text>
								<h5 className="mb-0">Price</h5>
								<p2>Php 40,000</p2>
							</Card.Text>
							<Button variant="primary">Enroll</Button>
						</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}